# Material from the training session on 20-06-2023

## Deploy a pod, full example, with GPU

We will deploy a pod with a service in front of it, an ingress that defines how to contact the service, a certificate on that ingress controller, and a GPU on it.

`kubectl apply -f ./01-gpu.yaml`

## Deploy a pod with a volume attached

We create a persistent volume claim that will request a volume from one of our Storage Backend (NetApp).

`kubectl apply -f ./02-pvc.yaml `
`kubectl apply -f ./02-pod-pvc.yaml`

## Deploy a webserver that will serve static html/files from an S3 bucket

`kubectl apply -f ./03-s3-website.yaml`

There is also a script that syncs a directory to the bucket.

`cat 03-s3-website-sync-share.sh`

## Job that computes Pi number

`kubectl apply -f ./04-job-pi.yaml`

## Cronjobs that schedule your crons

`kubectl apply -f ./05-cronjob.yaml`

## Bootstrap a MySQL cluster in HA mode with a single line using an operator

`cat 06-mysql-db.sh`

## Service type LoadBalancer

`kubectl apply -f ./08-loadbalancer.yaml`

## Shiny app

This helm chart you can use it to deploy your own shiny application.

```
helm repo add  emblshiny https://sourcecode.embl.de/api/v4/projects/280/packages/helm/stable

helm show values emblshiny/shiny

helm upgrade  --install -n hentze-group rbpbase emblshiny/shiny -f ./values.yaml
```

## Pipelines

You can check: https://git.embl.de/grp-itservices/kubernetes_yaml_pipeline 
