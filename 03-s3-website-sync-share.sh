#!/bin/bash
# Check is Lock File exists, if not create it and set trap on exit
 if { set -C; 2>/dev/null >/tmp/.manlocktest.lock; }; then
         trap "/usr/bin/rm -f /tmp/.manlocktest.lock" EXIT
 else
         /usr/bin/echo "Lock file exists… exiting"
         exit
 fi

set +o noclobber

/home/whuber/bin/rclone sync -v   --update --ignore-errors /g/huber/www-huber embl:www-huber/ 2>/g/huber/www-huber.err
