#!/bin/bash

export KUBECONFIG=/home/xemacs/.kube/config

helm repo add percona https://percona.github.io/percona-helm-charts/
helm repo update
helm upgrade --install  --debug -n tenant-front my-db percona/pxc-db \
      	--set pxc.volumeSpec.resources.requests.storage=20Gi \
      	--set backup.enabled=false --set pxc.resources.limits.memory=1G \
       	--set pxc.resources.limits.cpu=1 --set haproxy.resources.limits.cpu=1 \
       	--set haproxy.resources.limits.memory=1G \
       	--set logcollector.resources.limits.memory=1G \
       	--set logcollector.resources.limits.cpu=1 \
       	--set pxc.sidecarResources.requests.cpu=1 \
       	--set pxc.sidecarResources.requests.memory=1G \
       	--set haproxy.sidecarResources.limits.cpu=1 --set haproxy.sidecarResources.limits.memory=1G
